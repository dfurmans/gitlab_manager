from GitLabManager.common import *

class MemberView:
    member_data: MemberData = None
    gl_member: gitlab

    def __init__(self, gl_member: gitlab):
        self.gl_member = gl_member

    def _find_user(self, name: str):
        user_obj = None
        user = self.gl.users.list(username=name)
        
        if user:
            user_obj = self.gl.users.get(user[0].id)
        else:
            print("This user doesn't exists!!")

        return user_obj
        
    def get_member(self, name: str):
        user = self._find_user(name)
        gl_member = None

        if user and self.gl_member:
            user_id = user.get_id()            
            
            try:
                gl_member = self.gl_member.members.get(user_id)
            except:
                print("Error: this user is not a member\n")
                gl_member = None
       
        return gl_member
        

    def get_members(self):
        members = None

        if self.gl_member:
            members = self.gl_member.members.all()

        return members

    def get_owner(self):
        member_list = self.get_members()
        owner = None

        if member_list:
            if type(self.gl_member) is gitlab.v4.objects.Group:
                #in a group, can be more than one owners
                owner = []

                for member in member_list:
                    #the owners of a group has a access level equal or higher than 50
                    if member.access_level >= 50:
                        owner.append(member.username)
            else:
                #in a project, the owner is the creator of the repository
                owner = self.gl_member.namespace['path']

        return owner
        
class MemberAdmin(MemberView):
    gl_member: gitlab
    
    def __init__(self, gl_member: gitlab):
        self.gl_member = gl_member
        super().__init__(gl_member)
    
    def add_member(self, member_data: MemberData):
        success = False
        
        user = super()._find_user(member_data.username)

        if user:
            try:
                self.gl_member.members.create({'user_id': user.get_id(), 
                                               'access_level': member_data.access_level})
                success = True
            except Exception as error:
                print(f"Error adding member {member_data.username}: {error}")
                success = False
        
        return success
                                           
    def change_member_access_level(self, member_data: MemberData):
        success = True
        member = super().get_member(member_data.username)
            
        if member:
            member.access_level = member_data.access_level
            member.save()
        else:
            success = False
            
        return success


    def remove_member(self, username: str):
        success = True
        member = super().get_member(username)
        
        if member:
            member.delete()
        else:
            success = False
        
        return success
